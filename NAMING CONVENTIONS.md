# NAMING CONVENTIONS for Bocoup

## Active Files (Google Drive)

**FOLDER STRUCTURE:**

BOCOUP  
   - Active Project Management  
     - Consulting Projects  
       - Client 
         - Project  
           - \[JOURNAL] Journal  
           - \[ROADMAP] Roadmap  
           - Meeting agendas  
           - \[SOW] SOW  
           - \[FORM] Retrospective Form  
           - Any other useful / related files  
         - Project 
           - (same files as above)  
       - Client 
         - Project 
     - Internal Projects  

## Inactive Files (NAS)

-  Keeps the same structure as the Active Files  
-  organized inside an "Accounts" folder, for the most part


## GitHub

### Repositories

### Branches

### 