#Project Management OUTLINE

This is a chronological outline of the steps and tasks for managing Bocoup projects.  


## Before project start date

### Prep & setup

1. Collect SOW (preferably the signed version) from AM, (generally through the Ops@bocoup.com email address).  It contains:  
  a.  the project's start date    
  b.  the contract's end date (which may be _after_ the planned end date for the project)  
  c.  the number of dev-weeks and calendar weeks allowed to be spent for the project -- these are based on estimates created by AM and developers, and will determine the actual planned end-date.  

2. Create a folder in Google Docs --> Bocoup --> Active Project Management --> Consulting Projects   
  a. If the project is Internal, use the Internal Projects folder instead of Consulting Projects.  (Internal projects will not have an SOW, and the stakeholders will be Bocoup managers, but otherwise the process is the same.)   
  b. Name the folder after the _client_, with subfolders within it for each project (since we often have multiple projects per client). 
  c. Copy the contents of the [New Project Template Folder](https://drive.google.com/open?id=1emA9YI3RPG7fJ0YfO944LM-maVB6jxbP) to your new folder in their entirety.  This will give you a usable blank for each common type of file needed.  
  d. Be sure the permissions of the project folder AND its contents are set to allow all of Bocoup to find and view the files.  Explicitly give editing rights to the members of the project team.  Do NOT give anyone outside Bocoup access to the whole folder.  (Sometimes clients or outside collaborators are given access to individual files, but never to folders.)  
    1.  Particularly, it's common and desirable to give the client access to the Daily Journal and/or Roadmap, and to use these together during weekly plannings and checkins.  The Daily Journal makes a nice place to put agendae & notes for weekly checkin meetings.
  e. Put the SOW into the project folder, using the naming convention (so the first letters should be [SOW])  

3. Use the SOW to create a Project Brief based on the template.  Name it according to the [naming convention for Bocoup project documents]().  
	a.  Share this document immediately with the AM, and fill out what is known right now immediately.  
	b.  Ensure that the number of days/weeks and developers is filled out

4. Schedule (or begin the process of scheduling) the following three meetings.  See the next section for more details about them.

  a. A **Pre-launch Info Session**  
      - attendees: PM + developers  
	  - this happens BEFORE project launch  

  b. An **Internal Kickoff**  
      - attendees: PM, developers, AM, Director
	  - this happens BEFORE OR ON the first day of the project, preferably early in the day

  c. A **Client Kickoff** 
      - attendees: all of the people in b. plus the Product Owner (and any other people desired) on the client side
      - this ideally happens ON the first day of the project, but may sometimes need to be put off until a few days later.  If the Client Kickoff is delayed at all, AM & PM must work with the customer to identify an appropriate number of days of early work that can be done before Kickoff.
	  - _The Client Kickoff should not happen without a completed and signed SOW.  Sometimes the signing process gets held up, so work with the AM to be 100% sure the SOW is signed before kickoff._

5.  Add a tab to the [Project Schedules Master]() spreadsheet, and input the start and end dates and number of days from #1.  Set up a blank schedule following the format of the other tabs.

### Pre-&-Launch meeting details / process

1. The pre-launch info session is held before both project kickoffs & the start date.  The PM & assigned developers review the SOW and complete the project brief together.
  - Make sure all questions for the client, questions for the AM, and any potential red or yellow flags are recorded in this meeting.  _This meeting is held separately to ensure that there is adequate time to discuss the project BEFORE it starts.  This is not the same as the Internal Project Kickoff meeting; do not combine them!_  Some questions in the Brief, such as how the team wants to do sprints or check-in meetings, are OK to wait until the Internal Kickoff, if necessary.  This meeting is primarly to discuss scope, process, handoff, and to surface questions for the AM/Director/Sales team amd clients.

2.  Share the now-more-complete Project Brief with the AM and Director.  Make sure they are aware of questions and any warning-signs the team has identified, so that as many of these can be addressed before the project launches as possible.

3.  Hold any pre-project discussions (face-to-face or async) requested by the development team in the meeting in #1.  Be sure they have adequate time to clarify the scope and technical requirements with the client, and to ask the Director or AM for clarification about deliverables, scope limitations (i.e. future work to not venture into "for free"), etc.

4.  For the Internal Kickoff, the agenda is two parts:  
  - First, review the Project Brief together and fill in any missing information.  Where there are decisions that can't be made without the client's input (such as the time/date of client meetings), make sure these are noted in the Client Kickoff Agenda.  
  - Then review the Client Kickoff Agenda in its entirety and make sure it contains all the questions we want to ask the client, and that it fits into the time allotted.

## Project start

1.  Hold an Internal Kickoff meeting before (or early on) the project's start-date.  The development team, AM, PM, and Director should all review the Project Brief together, and then look at the Client Kickoff Agenda in detail, to be sure that all the necessary items and questions are on it.  After that meeting, schedule all internal project meetings (sprint plannings, daily checkin, etc.) according to the cadence the team decides on.  Also use the Templates in the folder to create a Roadmap, Journal, and other docs as needed.

2.  Hold the Client Kickoff meeting on the project's first day (ideally).  After that meeting, schedule all of the regular checkins, handoff meetings, and other client-facing activity according to the cadence and dates/times the group decides on.

3.  After the Kickoffs, use the template (in the project folder) to create and share a Retrospective Form with the team. Ideally, people will fill this out periodically throughout the project (it can be filled out multiple times and/or responses edited).  

## Daily & weekly

1.  Every project must have a **daily check-in meeting**.  (For one-person projects, the meeting will be with a manager.)  It can be in any format, but it must include a substantive update from everyone on what was accomplished yesterday and what is planned for today (or what was accomplished today and what's planned for tomorrow, if the meeting is late in the day).  

2.  Every project team must update their **Daily Project Journal** before 11:30 am each day.  This can be done during the daily checkin meeting, or separately.  If the team meets late in the afternoon, they can update the journal with a "plan for tomorrow", and update the previous day with a "what really happened".

3.  Each project must have a **weekly Planning meeting** to update their roadmap, _with estimates_, and plan the next week's work.  Sometimes this meeting includes the client, in which case the Roadmap updates should be finished before the meeting starts (teams may choose to have a short internal meeting for this, prior to the client meeting).  

4.  Most projects will have a weekly (or per-sprint) meeting with the client to check in, ask questions, unblock anything that's blocked, etc.  


## Mid-project

1.  **Mid-project retrospectives** should be held approximately every month -- more often if the project has many workstreams, many contractors, or any other complexities.  Use the same Project Retrospective document for all retrospective meetings (so that at the end of the project, there is one document containing all our retrospective knowledge).  During the Mid-Project Retrospective, questions can be added for later discussion, or the group can simply go through the "STOP / START / CELEBRATE" sections and add their mid-project thoughts.  These are purely discussion-based meetings and do not include a separate feedback portion (that happens in the End-of-Project Retrospective).

2.  Any time a new engineer onboards into an existing project, hold an **Onboarding Kickoff** for the team (dev team minimum).  Provide the Project Brief to the new engineer beforehand so that they can read it and have questions ready for the team at the meeting.

3.  Some projects benefit from **blog posts or public updates** as they make progress -- typically for small projects, this can happen at the end, but for larger ones, _one formal post or report per quarter_ is a good goal.


## 80% complete (by days delivered)

1.  The Account Manager should schedule a check-in with the client, to get feedback on the project so far, and to surface and discuss future work.  

2.  The PM should take as many of the following steps as are possible / applicable:  
    a.  Inform the team of how much time is left, and ask everyone to raise any concerns they have about the due/handoff date;  
    b.  Get a fresh / updated set of estimates on the work remaining.  
      1.  If there's any possibility of not finishing on time, alert the AM immediately, and schedule a discussion with the AM and the devs to choose solutions.  
      2.  If it looks like there's going to be extra time (the project might finish before the last day), collect ideas for additional things to add, and estimates for each.  Bring these to the AM and, if needed, schedule time for the AM and devs to discuss & choose from among them.  
    c.  Remind the team to capture their thoughts, concerns, & questions in the Retro Form.  


## Project end

1.  Most projects (but not all; it depends on client preference and the type of work) will involve a **Handoff meeting**.  If at all possible, this should be scheduled for the last day of the project.  In special circumstances, it can be another day instead.  
  a.  There is no set format or agenda for a handoff meeting.  Some may include a demonstration, while others may involve delivery of code or documentation.  Create an agenda based on the needs of the client and project.  Be sure there's adequate time at the end for questions and clarifications.  
  b.  Be aware of scope-creep, and be sure that all potential _future work_ is labeled as such, and that none of it is begun without the approval of the AM.  

2.  Within 2 weeks (preferably less) after the end of the project, schedule an **End-of-Project Retrospective**.  End-of-project retrospectives, unless they are very small, benefit strongly from having a Facilitator who is not a member of the project team (not the PM or AM for the project).  
   a.  Use the same Retrospective document as the Mid-Project Retrospectives  
   b.  Remind the team a few days before the Retrospective that they _must_ now finish filling in the Retro Form, if they haven't yet.  
   c.  Look at that document to get an idea how long the End-of-Project Retrospective should be:  
     1.  If there are many people involved, increase the time  
     2.  If the project was long, involved many workstreams, or had any serious issues, increase the time  
     3.  If there are a lot of questions for discussion in the Retrospective document (added either during Mid-Project Retrospectives or as a result of people's Retro Form answers), increase the time so they can be discussed  
     4.  Add 10 minutes of time per person (including everyone, not just devs) to the end of the discussion portion, for 360 Feedback.  See the Retrospective document for instructions for the facilitator in scheduling these feedback sessions.


## Post-project

1.  Collect all of the action items from the Retrospective (there will usually be many) and put them in the proper places for follow-up (schedule a meeting, create Github issues, put them on someone's task list, etc.)  
  a.  Pay special attention to blog posts / case studies and follow-up with the client or technical community:  These should be tracked (typically in a GH issue) and followed-up on to ensure they don't get forgotten.

2.  Share the Retrospective document with the Coop (coop@bocoup.com) to give everyone a chance to read and learn from it.

3.  Move the ENTIRE project folder from Google Docs to the NAS.




# Glossary / abbreviations
AM = Account manager  
PM = Project manager  
SOW = Statement of Work  

